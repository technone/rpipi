extern crate phf;
extern crate rppal;
extern crate ureq;

use std::collections::HashMap;
use std::collections::vec_deque::VecDeque;
use std::sync::mpsc::{Receiver, sync_channel};
use std::thread;
use std::time::Duration;

use phf::phf_map;
use rppal::gpio::{Gpio, OutputPin};

use svn_seg::SevenSeg;

mod svn_seg;
mod digit_getter;
mod resilience;

static SEGMENT_TO_PIN: phf::Map<char, u8> = phf_map! {
	'a' => 3,
    'b' => 5,
    'c' => 7,
    'd' => 8,
    'e' => 10,
    'f' => 11,
    'g' => 12,
    'p' => 13,
};

pub struct PiDequeue {
	queue: VecDeque<u8>,
	chunk_size: usize,
	digit_recv: Receiver<Vec<u8>>,
}


impl PiDequeue {
	pub fn new_start(chunk_size: usize, start_digit: usize) -> Result<Self, Box<dyn std::error::Error>> {
		let (digit_snd, digit_recv) = sync_channel(0);

		let c_z = chunk_size.clone();
		thread::spawn(move || digit_getter::main_routine(c_z, digit_snd, start_digit));

		let mut pdq = Self {
			queue: VecDeque::with_capacity(chunk_size),
			chunk_size,
			digit_recv,
		};

		pdq.get_more_async()?;
		Ok(pdq)
	}

	pub fn new(chunk_size: usize) -> Result<Self, Box<dyn std::error::Error>> {
		Self::new_start(chunk_size, 0)
	}

	fn get_more_async(&mut self) -> Result<(), Box<dyn std::error::Error>> {
		println!("Requesting {} digits from digit getter.", self.chunk_size);

		let digits = self.digit_recv.recv()?;
		self.queue.append(&mut digits.into_iter().collect());

		println!("Got the digits from getter.");

		Ok(())
	}
}

impl Iterator for PiDequeue {
	type Item = u8;

	fn next(&mut self) -> Option<Self::Item> {
		if self.queue.is_empty() {
			println!("Queue currently empty.");
			self.get_more_async().ok();
		}

		self.queue.pop_front()
	}
}

fn seg_to_bcm(x: char, pin_to_bcm: &HashMap<u8, u8>) -> u8 {
	pin_to_bcm[&SEGMENT_TO_PIN[&x]]
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
	println!("Welcome to rpipi.");

	// Can't convert pin_to_bcm to a phf map because u8 can't be keys.
	let pin_to_bcm: HashMap<u8, u8> = [(3, 2), (5, 3), (7, 4), (8, 14), (10, 15),
		(11, 17), (12, 18), (13, 27)].iter().cloned().collect();

	let s_b = |c: char| seg_to_bcm(c, &pin_to_bcm);

	let gp = Gpio::new()?;
	let mut displ: SevenSeg<OutputPin> = SevenSeg::new(gp.get(s_b('a'))?.into_output(),
													   gp.get(s_b('b'))?.into_output(),
													   gp.get(s_b('c'))?.into_output(),
													   gp.get(s_b('d'))?.into_output(),
													   gp.get(s_b('e'))?.into_output(),
													   gp.get(s_b('f'))?.into_output(),
													   gp.get(s_b('g'))?.into_output());

	let mut dot = gp.get(s_b('p'))?.into_output();
	dot.set_low();

	let res = resilience::Resilience::new()?;

	let pd = PiDequeue::new_start(20, res.read()?)?;

	let one_sec = Duration::from_secs(1);
	let mut previous_digit: u8 = 10;

	for (counter, d) in pd.into_iter().enumerate() {
		if d == previous_digit { // for consecutive numbers, flip the dot
			dot.toggle();
		} else if dot.is_set_high() { // for non-consecutive numbers, turn it off if it's on
			dot.set_low();
		}

		displ.clear();
		println!("Digit: {}", d);
		displ.display(d);
		thread::sleep(one_sec);

		res.write(counter)?;

		previous_digit = d;
	}

	Ok(())
}

