/// Ripped off from Sevensegment, made to work with RPPAL. The wiring for the digits was wrong.
/// If the structure is sub-optimal, blame the sevensegment writer.
extern crate rppal;

use rppal::gpio::OutputPin;

enum Segment {
	A,
	B,
	C,
	D,
	E,
	F,
	G,
}

fn seg_to_index(seg: Segment) -> usize {
	match seg {
		Segment::A => 0,
		Segment::B => 1,
		Segment::C => 2,
		Segment::D => 3,
		Segment::E => 4,
		Segment::F => 5,
		Segment::G => 6,
	}
}

/// A structure representing the 7 segments of a 7-segment display
pub struct SevenSeg<T> {
	segments: [T; 7],
}

impl SevenSeg<OutputPin>
{
	pub fn new(seg_a: OutputPin, seg_b: OutputPin, seg_c: OutputPin, seg_d: OutputPin, seg_e: OutputPin, seg_f: OutputPin, seg_g: OutputPin) -> Self {
		Self {
			segments: [seg_a, seg_b, seg_c, seg_d, seg_e, seg_f, seg_g]
		}
	}

	fn change_state(&mut self, which: Vec<Segment>, state: bool) {
		for w in which {
			let pin = &mut self.segments[seg_to_index(w)];

			if !state && pin.is_set_high() {
				pin.set_low();
			} else if state && pin.is_set_low() {
				pin.set_high();
			}
		}
	}

	pub fn clear(&mut self) {
		self.change_state(vec![Segment::A, Segment::B, Segment::C, Segment::D,
							   Segment::E, Segment::F, Segment::G], false);
	}

	/// https://www.nutsvolts.com/uploads/wygwam/NV_0501_Marston_Figure02.jpg
	pub fn display(&mut self, num: u8) {
		self.clear();

		match num {
			0 => self.change_state(vec![Segment::A, Segment::B, Segment::C, Segment::D, Segment::E, Segment::F], true),
			1 => self.change_state(vec![Segment::B, Segment::C], true),
			2 => self.change_state(vec![Segment::A, Segment::B, Segment::D, Segment::E, Segment::G], true),
			3 => self.change_state(vec![Segment::A, Segment::B, Segment::C, Segment::D, Segment::G], true),
			4 => self.change_state(vec![Segment::B, Segment::C, Segment::F, Segment::G], true),
			5 => self.change_state(vec![Segment::A, Segment::C, Segment::D, Segment::F, Segment::G], true),
			6 => self.change_state(vec![Segment::A, Segment::C, Segment::D, Segment::E, Segment::F, Segment::G], true),
			7 => self.change_state(vec![Segment::A, Segment::B, Segment::C], true),
			8 => self.change_state(vec![Segment::A, Segment::B, Segment::C, Segment::D, Segment::E, Segment::F, Segment::G], true),
			9 => self.change_state(vec![Segment::A, Segment::B, Segment::C, Segment::F, Segment::G], true),
			_ => self.clear()
		}
	}
}
