use std::error::Error;
use std::path::{PathBuf};
use std::fs::{File, OpenOptions};
use std::io::{BufReader, BufWriter, Write};
use std::env;

extern crate bincode;

static FILENAME: &str = ".rpipi_resilience";

pub(crate) struct Resilience {
	location: PathBuf,
}

impl Resilience {
	pub(crate) fn new() -> Result<Self, Box<dyn Error>> {
		// The script is run from rc.local but the environment variables setup by .bashrc.
		// Thus, if you run the script at startup, invocation of HOME will fail and get defaulted.
		// If you run the script normally from the terminal, HOME exists and is used here.
		let home_string = env::var("HOME").unwrap_or(String::from("/home/pi"));
		let pb: PathBuf = [&home_string, FILENAME].iter().collect();

		if !pb.is_file() {
			let f = File::create(&pb)?;
			bincode::serialize_into(&mut BufWriter::new(&f), &(0 as usize))?;
		}

		Ok(Self {
			location: pb
		})
	}

	pub(crate) fn read(&self) -> Result<usize, Box<bincode::ErrorKind>> {
		let file = File::open(&self.location).unwrap();
		bincode::deserialize_from(BufReader::new(file))
	}

	pub(crate) fn write(&self, num: usize) -> Result<(), Box<bincode::ErrorKind>> {
		let file = OpenOptions::new().read(true).write(true).open(&self.location).unwrap();
		let mut writer = BufWriter::new(file);
		bincode::serialize_into(&mut writer, &num)?;
		writer.flush().unwrap();
		Ok(())
	}
}