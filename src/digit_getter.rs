extern crate ureq;

use std::collections::HashMap;
use std::sync::mpsc::SyncSender;
use std::time::Duration;
use std::thread;

fn string_to_digits(input: &str) -> Vec<u8> {
	input.chars().filter_map(|s| s.to_digit(10)).map(|d| d as u8).collect::<Vec<u8>>()
}

pub(crate) fn main_routine(chunk_size: usize, snd: SyncSender<Vec<u8>>, start_digit: usize) {
	println!("Launched digit getter.");

	let mut next_digit_index = start_digit;

	loop {
		println!("Digit getter getting new digits.");
		let mut waiter = Waiter::new(Duration::from_secs(0),
									 Duration::from_secs(20),
									 Duration::from_secs(1));
		let resp = make_query_with_retry(&mut waiter, &next_digit_index, &chunk_size);

		let digits = string_to_digits(&resp.into_json_deserialize::<HashMap<String, String>>().unwrap()
			.get("content").ok_or_else(|| "No content in response body")
			.unwrap_or(&String::from("6")));

		next_digit_index = next_digit_index + chunk_size;

		println!("Digit getter got new digits, waiting for receive call.");
		snd.send(digits).expect("Could not send digits from getter.");
		println!("Digit getter has sent the digits.");
	}
}

struct Waiter {
	start: Duration,
	maximum: Duration,
	step: Duration,
}

impl Waiter {
	fn new(start: Duration, maximum: Duration, step: Duration) -> Waiter {
		Self { start, maximum, step }
	}

	fn step(&mut self) -> Duration {
		if (self.start + self.step) > self.maximum {
			self.maximum
		} else {
			self.start += self.step;
			self.start
		}
	}
}

fn make_query_with_retry(waiter: &mut Waiter, next_digit_index: &usize, chunk_size: &usize) -> ureq::Response {
	let resp = ureq::get("https://api.pi.delivery/v1/pi")
		.query("start", &next_digit_index.to_string())
		.query("numberOfDigits", &chunk_size.to_string())
		.call();

	if resp.ok() {
		resp
	} else {
		let wait_for = waiter.step();

		println!("Request error {}: {}", resp.status(), resp.into_string().unwrap());
		println!("Invalid response. Waiting for {:?} before re-attempting.", wait_for);
		thread::sleep(wait_for);

		make_query_with_retry(waiter, &next_digit_index, &chunk_size)
	}
}

