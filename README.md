h2. rPiPi

Uses the Google Pi API to display digits of Pi on a 7-segment display. It waits one second between digits. The dot is flashed when digits are consecutive. The current digit index is always saved, so the Raspberry Pi continues from where it left on restart.

API requests are done asynchronously from a separate thread. The thread retries on failure. It communicates with the main thread using a one-cell buffer.

The digits are retrieved in chunks and stored in a custom queue that refills itself when running out of digits. The custom queue implements an iterator, so getting digits is as easy as looping through the variable.

This project is coded specifically for a Raspberry Pi & has to be cross-compiled into an executable that can run on one to work. This also informed some of the choices with respect to external crates.
