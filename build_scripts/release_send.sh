#!/bin/bash

cargo build --release
ssh -t pi@raspberrypi.local 'sudo pkill -f rpipi'
scp target/arm-unknown-linux-gnueabihf/release/rpipi pi@raspberrypi.local:
ssh pi@raspberrypi.local './rpipi'
