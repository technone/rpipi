#!/bin/bash

cargo build
ssh -t pi@raspberrypi.local 'sudo pkill -f rpipi'
scp target/arm-unknown-linux-gnueabihf/debug/rpipi pi@raspberrypi.local:
ssh pi@raspberrypi.local './rpipi'
